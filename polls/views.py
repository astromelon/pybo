# Create your views here.
from socket import socket

from django.http import HttpResponse
import socket


def index(request):

    ips = socket.gethostbyname(socket.getfqdn())
    word = "Hello, world. This is Dev Environment Test Page! " + str(ips)

    return HttpResponse(word)
