from .base import *

ALLOWED_HOSTS = ['13.209.150.130']
STATIC_ROOT = BASE_DIR / 'static/'
STATICFILES_DIRS = []
DEBUG = False